using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace dot_net_perf.Controllers
{
    public class TestController : Controller
    {
        [Route("hello")]
        public String Hello()
        {
            return "Hello Word";
        }

        [Route("numeros-primos")]
        public String NumerosPrimos()
        {
            var number = 0;
            var numberOfPrimes = 0;
            while(true) {
                if(PrimeNumber.IsPrime(++number))
                    numberOfPrimes++;

                if(numberOfPrimes == 1000000)
                    break;
            }
            return String.Format("Prime: {0}", number);
        }

        [Route("video")]
        public String Video()
        {
            string filePath = System.IO.Path.GetFullPath("wwwroot/video.mp4");
            int counter = 1;
            while(counter <= 6) 
            {
                var file = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(file);
                
                int readCounter = 0;
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    readCounter++;
                }
                reader.Dispose();
                file.Dispose();
                counter++;
            }

            return String.Format("Video read {0} times", --counter);
        }
    }

}