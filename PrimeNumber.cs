using System;

namespace dot_net_perf.Controllers
{
    public static class PrimeNumber
    {
        public static bool IsPrime(int number)
        {
            if(number < 2)
                return false;

            for (int i = 2; i < Math.Floor(Math.Sqrt(number)); i++)
            {
                if(number % i == 0)
                    return false;
            }
            
            return true;
        }
    }
}